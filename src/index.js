import './styles/index.scss';
import $ from "jquery";
import infiniteScroll from "./js/infiniteScroll";
import draggableImages from "./js/draggableImages";
import initTestData from "./js/initTestData";

$(document).ready(() => {
    // Бесконечный скролл
    infiniteScroll();

    // Инициализировать тестовые данные (добавить картинки на полотно)
    initTestData();

    // Возможность передвигать картинки
    draggableImages();
});

