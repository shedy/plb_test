import $ from "jquery";
import { getRandomInt } from "./utils";

export default function initTestData() {
    const sheet = $('.sheet');

    for (let i = 0; i < 30; i++) {
        // Контейнер для изображения
        let container = $(document.createElement('div'));
        container.addClass('imgContainer');
        container.css({
            left: getRandomInt(0, $(window).width()),
            top: getRandomInt(0, $(window).height() * 2)
        });

        // Само изображение (по умолчанию оно имеет 200 пикс ширину, это и есть его зум)
        let img = $(new Image());
        img.attr('src', `assets/images/${i%10}.jpg`);
        img.css({ width: 350, marginLeft: -125, marginTop: -125 });

        container.append(img);
        sheet.append(container);
    }
}