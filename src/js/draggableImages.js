import Hammer from "hammerjs";
import $ from "jquery";

/**
 * Инициализирует обработчики событий для перетягивания imgContainer
 * (а также при нажатии переключает обработчик pinch и pan на картинку)
 */
export default function draggableImages() {
    let imgContainers = document.getElementsByClassName('imgContainer');
    const fsUnselectHandler = $('.fullscreen-unselect-handler');

    $.each(imgContainers, (index, imgContainer) => {
        let hammer = new Hammer(imgContainer, { inputClass: Hammer.TouchInput });
        const img = $(imgContainer).children('img', { domEvents: true });
        let imgHammer = new Hammer(img[0]);

        /**
         * Включает жесты для текущей картинки
         * @param enable если true, то жесты картинки активны, если false, то жесты контейнеров
         */
        function enableGesturesForCurrentImage(enable) {
            hammer.get('pan').set({ enable: !enable });
            imgHammer.get('pinch').set({ enable: enable });
            imgHammer.get('pan').set({ enable: enable });
            $(imgContainer).toggleClass('selected', enable);
            fsUnselectHandler.toggleClass('active', enable);
        }

        // При нажатии на контейнер картинки мы можем её зумить, но не можем двигать контейнер
        hammer.on('tap', ev => {
            if (!$(imgContainer).hasClass('selected')) {
                enableGesturesForCurrentImage(true);

                fsUnselectHandler.click(() => {
                    enableGesturesForCurrentImage(false);
                    fsUnselectHandler.unbind('click');
                });
            }
        });

        let startTop = 0;
        let startLeft = 0;
        let startScrollTop = 0;
        let dragEnabled = false;
        let dragIgnore = false;

        hammer.on('panstart', ev => {
            startTop = parseInt(imgContainer.style.top);
            startLeft = parseInt(imgContainer.style.left);
        });

        // При горизонтальном пане включаем drag
        hammer.on('pan', ev => {
            // Проверяем, что горизонтальный пан (для него включено перемещение)
            if (!dragIgnore && (ev.additionalEvent === 'panleft' || ev.additionalEvent === 'panright')) {
                dragEnabled = true;
                // Включаем перехват всех направлений, чтобы ограничить скролл страницы при вертикальном пане
                hammer.get('pan').set({ direction: Hammer.DIRECTION_ALL });
            }

            if (!dragEnabled) {
                return;
            }

            imgContainer.style.left = startLeft + ev.deltaX + 'px';
            imgContainer.style.top = startTop + ev.deltaY + 'px';
        });

        hammer.on('panend', ev => {
            dragEnabled = false;
            dragIgnore = false;
            console.log('panend');
            hammer.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });
        });

        /* НАЧАЛО ОБРАБОТКИ ЖЕСТОВ ДЛЯ ИЗОБРАЖЕНИЯ */
        let width = img.width();
        let marginLeft = -125;
        let marginTop = -125;
        let height = img.height();

        function initHeight() {
            if (!height) {
                height = img.height();
            }
        }

        /**
         * Установить marginLeft для картинки, чтобы не было пустой области в контейнере
         * @param marginLeft
         */
        function setRestrictedImgMarginLeft(marginLeft) {
            if (marginLeft < 0) {
                $(img).css('margin-left', Math.min(0, Math.max(marginLeft, - (width - imgContainer.offsetWidth))) + 'px');
            } else {
                $(img).css('margin-left', Math.min(marginLeft, 0) + 'px');
            }
        }

        /**
         * Установить marginTop для картинки, чтобы не было пустой области в контейнере
         * @param marginTop
         */
        function setRestrictedImgMarginTop(marginTop) {
            if (marginTop < 0) {
                $(img).css('margin-top', Math.min(0, Math.max(marginTop, - (height - imgContainer.offsetHeight))) + 'px');
            } else {
                $(img).css('margin-top', Math.min(marginTop, 0) + 'px');
            }
        }

        imgHammer.on("pinch", e => {
            initHeight();

            if ( width * e.scale >= 200 ) {
                var i = img[0];
                i.style.width = (width * e.scale) + 'px';
                i.style.height = (height * e.scale) + 'px';
                setRestrictedImgMarginLeft(parseInt(i.style.marginLeft) * e.scale);
                setRestrictedImgMarginTop(parseInt(i.style.marginTop) * e.scale);
            }
        });

        imgHammer.on('pan', ev => {
            initHeight();

            setRestrictedImgMarginLeft(marginLeft + ev.deltaX);
            setRestrictedImgMarginTop(marginTop + ev.deltaY);
        });

        imgHammer.on('panend', () => {
            marginLeft = parseInt($(img).css('margin-left'));
            marginTop = parseInt($(img).css('margin-top'));
        });

        imgHammer.on("pinchend", e => {
            width = width * e.scale;
            height = height * e.scale;
            marginLeft = marginLeft * e.scale;
            marginTop = marginTop * e.scale;
        });

        hammer.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });
        imgHammer.get('pinch').set({ enable: false });
        imgHammer.get('pan').set({ direction: Hammer.DIRECTION_ALL, enable: false });

        let scrollTimer = null;

        Hammer.on(window, "scroll", function(ev) {
            hammer.get('pan').set({ enabled: false });
            dragEnabled = false;
            dragIgnore = true;

            if (scrollTimer) {
                clearTimeout(scrollTimer);
            }

            scrollTimer = setTimeout(() => {
                hammer.get('pan').set({ enabled: true });
                dragEnabled = false;
                dragIgnore = false;
            }, 500);
        });
    });
}