import $ from "jquery";

const INFINITE_SCROLL_INREMENT = 10000;

/**
 * Бесконечный скролл
 * При приближении экрана к краю (правому или нижнему) добавляет пиксели (в право или в низ) нашему полотну .sheet
 * Сделано именно для окна и именно для .sheet в качестве тестового, т.к. универсальность функции тут не требуется
 */
export default function infiniteScroll() {
    let onScroll = () => {
        const sheet = $('.sheet');
        const sheetHeight = sheet.height();

        if ($(document).scrollTop() + window.innerHeight > sheetHeight - 1000) {
            sheet.height(sheetHeight + INFINITE_SCROLL_INREMENT);
        }
    };

    $(window).bind('scroll', onScroll);
    $(window).bind('touchmove', onScroll);
}