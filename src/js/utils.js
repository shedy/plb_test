/**
 * Возвращает случайное целое число в заданном интервале.
 * Возвращаемое значение не менее min (или следующее целое число, которое больше min, если min не целое) и не более (или равно) max.
 * @param min
 * @param max
 * @returns {number}
 */
export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
}