# PlotBoard Test

### Предпросмотр на Github

Скопируйте полностью url ниже:

```http://mavo-dash.com:2082/plb/```

### Итоги выполнения

1. Разработано только для телефонов (на компьютере работает частично в Chrome в режиме эмуляции телефона)
2. Тестировал на Android Chrome (Samsung Galaxy S8); iOS Safari, Chrome (iPad)

### Задание:

1. The web page is a mobile-friendly infinite scrollable sheet. On this page, there are 30 photos 1920 x 1080 placed in some locations. The width of each photo not less than 1/3 of the page width. The photos can overlap with each other.

2. The page can be scrolled with the default up-down pan gestures. The photos can be moved around with the left-right pan gesture (for example, when I start to pan rightwards on the photo it detaches from the page and follows the finger even if the finger then goes up or down or left).

3. A photo can be selected by a tap. The selected photo shall be marked somehow. With a black frame, for example. The selected photo can be zoomed on with a pinch gesture. And cropped within its frame with pan gestures when zoomed. That means a zoomed photo can be panned in the selected frame to make different parts of the photo visible. The selection is removed from the photo by a tap outside the photo 

## Установка

Клонируйте репозиторий и запустите

```bash
npm i
```

## Использование

### Сервер для разработки

```bash
npm start
```

Запустится по адресу `localhost:8080`.

### Production билд

```bash
npm run build
```

## Используемые инструменты для сборки

- [webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [Sass](https://sass-lang.com/)
- [PostCSS](https://postcss.org/)

